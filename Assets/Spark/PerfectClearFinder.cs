using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;

namespace Hikari.Spark {
    public class PerfectClearFinder : IDisposable {
        private NativeList<PcPattern> patterns;
        private NativeList<PcPattern> results;
        [NativeDisableContainerSafetyRestriction] private NativeReference<bool> abort;

        private bool allocated;
        private JobHandle jobHandle;

        public void StartSearch(List<SparkPieceKind> queue, SparkPieceKind? hold) {
            AllocateMemory();
            
        }

        public void Abort() {
            abort.Value = true;
        }

        private void AllocateMemory() {
            if (!patterns.IsCreated) patterns = new NativeList<PcPattern>(1000, Allocator.Persistent);
            if (!results.IsCreated) results = new NativeList<PcPattern>(1000, Allocator.Persistent);
            if (!abort.IsCreated) abort = new NativeReference<bool>(Allocator.Persistent) {Value = false};
            
            allocated = true;
        }

        public void Dispose() {
            if (!allocated) return;
            abort.Value = true;
            jobHandle.Complete();
            patterns.Dispose();
            results.Dispose();
            abort.Dispose();
            allocated = false;
        }
    }
}