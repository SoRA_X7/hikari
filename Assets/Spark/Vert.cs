using System;

namespace Hikari.Spark {
    public readonly struct Vert : IEquatable<Vert> {
        // public readonly BitBoard board;
        // public readonly SparkPieceKind hold;
        private readonly ulong data;

        public BitBoard Board => new BitBoard(data & ((1UL << 60) - 1UL));
        public SparkPieceKind Hold => (SparkPieceKind) ((data >> 60) & 0xf);

        public Vert(BitBoard board, SparkPieceKind hold) {
            data = board.data | ((ulong) hold << 60);
        }

        public bool Equals(Vert other) {
            return data == other.data;
        }

        public override bool Equals(object obj) {
            return obj is Vert other && Equals(other);
        }

        public override int GetHashCode() {
            return data.GetHashCode();
        }

        public static bool operator ==(Vert left, Vert right) {
            return left.Equals(right);
        }

        public static bool operator !=(Vert left, Vert right) {
            return !left.Equals(right);
        }
    }
}