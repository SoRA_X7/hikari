using Unity.Collections;
using Unity.Jobs;

namespace Hikari.Spark.Jobs {
    public struct PcEnumerateJob : IJobParallelFor {
        [ReadOnly] public NativeReference<bool>.ReadOnly abort;
        [ReadOnly] public NativeArray<Placement> firstPlacements;
        [WriteOnly] public NativeList<PcPattern>.ParallelWriter results;
        
        public void Execute(int index) {
            
        }
    }
}