namespace Hikari.Spark {
    public enum SparkPieceKind : byte {
        None,
        I,
        O,
        T,
        J,
        L,
        S,
        Z
    }
}