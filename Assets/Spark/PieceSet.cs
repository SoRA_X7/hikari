using System;

namespace Hikari.Spark {
    public struct PieceSet : IEquatable<PieceSet> {
        internal uint data;

        public int this[int kind] {
            get => (int) ((data >> (kind * 4)) & 0xf);
            // set => data = (uint) ((value & 0xf) << (kind * 4));
        }
        public bool Equals(PieceSet other) {
            return data == other.data;
        }

        public override bool Equals(object obj) {
            return obj is PieceSet other && Equals(other);
        }

        public override int GetHashCode() {
            return unchecked((int) data);
        }

        public static bool operator ==(PieceSet left, PieceSet right) {
            return left.Equals(right);
        }

        public static bool operator !=(PieceSet left, PieceSet right) {
            return !left.Equals(right);
        }
    }
}