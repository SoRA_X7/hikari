using System;

namespace Hikari.Spark {
    public readonly struct BitBoard : IEquatable<BitBoard> {
        internal readonly ulong data;

        public BitBoard(ulong data) {
            this.data = data;
        }

        public BitBoard Combine(BitBoard other) {
            return new BitBoard(data | other.data);
        }

        public bool Overlaps(BitBoard other) {
            return (data & other.data) != 0;
        }

        public bool Equals(BitBoard other) {
            return data == other.data;
        }

        public override bool Equals(object obj) {
            return obj is BitBoard other && Equals(other);
        }

        public override int GetHashCode() {
            return data.GetHashCode();
        }

        public static bool operator ==(BitBoard left, BitBoard right) {
            return left.Equals(right);
        }

        public static bool operator !=(BitBoard left, BitBoard right) {
            return !left.Equals(right);
        }
    }
}