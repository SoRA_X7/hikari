using System;
using System.Diagnostics;
using Hikari.Optimizer.GA;
using Hikari.Optimizer.Runner;
using TMPro;
using UnityEngine;

namespace Hikari.Optimizer {
    public class MatchController : MonoBehaviour {
        private MatchMode mode;

        private VsCcMatchRunner runner;
        public MatchControllerStatus status { get; private set; }

        private Stopwatch stopwatch;

        private Monad monad;

        public int id;

        [SerializeField] private TMP_Text idText;
        [SerializeField] private TMP_Text statusText;

        public void Init(Monad monad, int i) {
            this.monad = monad;
            status = MatchControllerStatus.Running;
            statusText.text = "Running " + i;

            runner = gameObject.AddComponent<VsCcMatchRunner>();
            runner.weights = monad.weights;

            idText.text = "#" + id;
        }

        private void Update() {
            if (runner && runner.Ended) {
                monad.score = runner.reward;
                Cleanup();
            }
        }

        public void Cleanup() {
            if (runner) {
                Destroy(runner);
                runner = null;

                status = MatchControllerStatus.Finished;
                statusText.text = "Finished";
            }
        }

        public void ResetToIdle() {
            status = MatchControllerStatus.Idle;
            statusText.text = "Idle";
        }
    }
}