namespace Hikari.Optimizer {
    public enum MatchControllerStatus {
        Idle,
        Initializing,
        Starting,
        Running,
        Finished
    }
}