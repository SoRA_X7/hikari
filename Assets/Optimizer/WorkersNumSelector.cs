using System;
using TMPro;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;

namespace Hikari.Optimizer {
    public class WorkersNumSelector : MonoBehaviour {
        [SerializeField] private Brain brain;
        [SerializeField] private TMP_Dropdown dropdown;

        private void Start() {
            dropdown.ClearOptions();
            for (var i = 1; i <= JobsUtility.JobWorkerMaximumCount; i++) {
                dropdown.options.Add(new TMP_Dropdown.OptionData(i.ToString()));
            }

            dropdown.onValueChanged.AddListener(i => brain.workersNum = i + 1);

            dropdown.value = Math.Max(1, JobsUtility.JobWorkerMaximumCount - 3);
        }
    }
}