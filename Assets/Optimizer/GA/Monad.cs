using System;
using Hikari.AI.Eval;
using Random = UnityEngine.Random;

namespace Hikari.Optimizer.GA {
    public class Monad : IComparable<Monad> {
        public Weights weights;
        public float score;

        public Monad(Weights weights) {
            this.weights = weights;
        }

        private Monad() { }

        public static Monad Crossover(Monad m1, Monad m2) {
            var w1 = m1.weights;
            var w2 = m2.weights;
            return new Monad {
                weights = new Weights {
                    clear1 = CrossoverGene(w1.clear1, w2.clear1),
                    clear2 = CrossoverGene(w1.clear2, w2.clear2),
                    clear3 = CrossoverGene(w1.clear3, w2.clear3),
                    clear4 = CrossoverGene(w1.clear4, w2.clear4),
                    tSpin1 = CrossoverGene(w1.tSpin1, w2.tSpin1),
                    tSpin2 = CrossoverGene(w1.tSpin2, w2.tSpin2),
                    tSpin3 = CrossoverGene(w1.tSpin3, w2.tSpin3),
                    tMini1 = CrossoverGene(w1.tMini1, w2.tMini1),
                    tMini2 = CrossoverGene(w1.tMini2, w2.tMini2),
                    perfect = CrossoverGene(w1.perfect, w2.perfect),
                    tHole = CrossoverGene(w1.tHole, w2.tHole),
                    tstHole = CrossoverGene(w1.tstHole, w2.tstHole),
                    finHole = CrossoverGene(w1.finHole, w2.finHole),
                    b2bContinue = CrossoverGene(w1.b2bContinue, w2.b2bContinue),
                    b2bDestroy = CrossoverGene(w1.b2bDestroy, w2.b2bDestroy),
                    ren = CrossoverGene(w1.ren, w2.ren),
                    wastedT = CrossoverGene(w1.wastedT, w2.wastedT),
                    holdT = CrossoverGene(w1.holdT, w2.holdT),
                    bumpSum = CrossoverGene(w1.bumpSum, w2.bumpSum),
                    bumpSumSq = CrossoverGene(w1.bumpSumSq, w2.bumpSumSq),
                    maxHeight = CrossoverGene(w1.maxHeight, w2.maxHeight),
                    top50 = CrossoverGene(w1.top50, w2.top50),
                    top75 = CrossoverGene(w1.top75, w2.top75),
                    danger = CrossoverGene(w1.danger, w2.danger),
                    placementHeight = CrossoverGene(w1.placementHeight, w2.placementHeight),
                    moveTime = CrossoverGene(w1.moveTime, w2.moveTime),
                    bridge = CrossoverGene(w1.bridge, w2.bridge),
                    cavities = CrossoverGene(w1.cavities, w2.cavities),
                    cavitiesSq = CrossoverGene(w1.cavitiesSq, w2.cavitiesSq),
                    cavitiesMove = CrossoverGene(w1.cavitiesMove, w2.cavitiesMove),
                    overhangs = CrossoverGene(w1.overhangs, w2.overhangs),
                    overhangsSq = CrossoverGene(w1.overhangsSq, w2.overhangsSq),
                    overhangsMove = CrossoverGene(w1.overhangsMove, w2.overhangsMove),
                    coveredCells = CrossoverGene(w1.coveredCells, w2.coveredCells),
                    coveredCellsSq = CrossoverGene(w1.coveredCellsSq, w2.coveredCellsSq),
                    downstack = CrossoverGene(w1.downstack, w2.downstack),
                    holeDepth = CrossoverGene(w1.holeDepth, w2.holeDepth),
                    rowTransitions = CrossoverGene(w1.rowTransitions, w2.rowTransitions),
                    maxHeightDiff = CrossoverGene(w1.maxHeightDiff, w2.maxHeightDiff),
                    wellX = new int10(
                        CrossoverGene(w1.wellX.item0, w2.wellX.item0),
                        CrossoverGene(w1.wellX.item1, w2.wellX.item1),
                        CrossoverGene(w1.wellX.item2, w2.wellX.item2),
                        CrossoverGene(w1.wellX.item3, w2.wellX.item3),
                        CrossoverGene(w1.wellX.item4, w2.wellX.item4),
                        CrossoverGene(w1.wellX.item5, w2.wellX.item5),
                        CrossoverGene(w1.wellX.item6, w2.wellX.item6),
                        CrossoverGene(w1.wellX.item7, w2.wellX.item7),
                        CrossoverGene(w1.wellX.item8, w2.wellX.item8),
                        CrossoverGene(w1.wellX.item9, w2.wellX.item9)
                    )
                }
            };
        }

        private static int CrossoverGene(int g1, int g2) {
            var rand = Random.Range(0, 100);
            if (rand < 40) return g1;
            if (rand < 80) return g2;
            if (rand < 92) return (g1 + g2) / 2;
            return Rand();
        }

        public static Monad CreateRandom() {
            return new Monad(new Weights {
                clear1 = Rand(),
                clear2 = Rand(),
                clear3 = Rand(),
                clear4 = Rand(),
                tSpin1 = Rand(),
                tSpin2 = Rand(),
                tSpin3 = Rand(),
                tMini1 = Rand(),
                tMini2 = Rand(),
                perfect = Rand(),
                tHole = Rand(),
                tstHole = Rand(),
                finHole = Rand(),
                b2bContinue = Rand(),
                b2bDestroy = Rand(),
                ren = Rand(),
                wastedT = Rand(),
                holdT = Rand(),
                bumpSum = Rand(),
                bumpSumSq = Rand(),
                maxHeight = Rand(),
                top50 = Rand(),
                top75 = Rand(),
                danger = Rand(),
                placementHeight = Rand(),
                moveTime = Rand(),
                bridge = Rand(),
                cavities = Rand(),
                cavitiesSq = Rand(),
                cavitiesMove = Rand(),
                overhangs = Rand(),
                overhangsSq = Rand(),
                overhangsMove = Rand(),
                coveredCells = Rand(),
                coveredCellsSq = Rand(),
                downstack = Rand(),
                holeDepth = Rand(),
                rowTransitions = Rand(),
                maxHeightDiff = Rand(),
                wellX = new int10(Rand(), Rand(), Rand(), Rand(), Rand(), Rand(), Rand(), Rand(), Rand(), Rand())
            });
        }

        private static int Rand() => Random.Range(-5000, 5000);

        public int CompareTo(Monad other) {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return score.CompareTo(other.score);
        }
    }
}