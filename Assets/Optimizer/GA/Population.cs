using System.Collections.Generic;
using System.IO;
using System.Linq;
using Hikari.AI.Eval;
using Hikari.Utils;
using UnityEngine;

namespace Hikari.Optimizer.GA {
    public class Population {
        public int generation;
        public List<Monad> monads = new List<Monad>();

        public Population(int generation) {
            this.generation = generation;
        }

        public Population(int generation, IEnumerable<Monad> monads) {
            this.generation = generation;
            this.monads.AddRange(monads);
        }

        public Population Next() {
            monads.Sort();
            monads.Reverse();
            var next = new List<Monad>();
            next.AddRange(monads);

            var weighted = new WeightedIndex(next.Select((m,i) => (m.score + 1) / (i + 1)));

            for (var i = 5; i < next.Count; i++) {
                var p1 = weighted.Sample(Random.Range(0f, 1f));
                var p2 = p1;
                while (p1 == p2) {
                    p2 = weighted.Sample(Random.Range(0f, 1f));
                }

                next[i] = Monad.Crossover(monads[p1], monads[p2]);
            }
            
            return new Population(generation + 1, next);
        }

        public static Population CreateInitialPopulation(int len) {
            var pop = new Population(0);
            pop.monads.Add(new Monad(Weights.Default));
            for (var i = 0; i < len; i++) {
                pop.monads.Add(Monad.CreateRandom());
            }

            return pop;
        }

        public void Save() {
            var path = Path.Combine(Application.persistentDataPath, "best-" + generation + ".json");

            using var file = File.CreateText(path);
            file.Write(JsonUtility.ToJson(monads.OrderByDescending(m => m.score).First()));
        }
    }
}