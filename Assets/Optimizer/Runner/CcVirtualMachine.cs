using System;
using System.Collections.Generic;
using System.Linq;
using ColdClearSharp;
using Hikari.Puzzle;
using UnityEngine;

namespace Hikari.Optimizer.Runner {
    public class CcVirtualMachine : IController, IDisposable {
        private ColdClearBot bot;
        private Game game;

        private LinkedList<CCMovement> instructions = new LinkedList<CCMovement>();
        private bool holdRequired;
        private bool firstHold;
        private CCMovement? lastMove;

        private bool requestMove;
        private bool harddrop;
        private bool dead;

        public CcVirtualMachine(Game game) {
            this.game = game;
            bot = new ColdClearBot();
            game.Controller = this;
        }
        
        public void Start() {
            game.OnPieceSpawned += _ => {
                if (firstHold) return;
                
                requestMove = true;
                bot.RequestNextMove(game.Damage);
            };
            game.OnQueueUpdated += e => {
                bot.AddNextPiece(ConvertCcPiece(e.kind));
            };
            game.OnGarbageLinesAdded += _ => {
                Debug.Log("CC reset");
                bot.Reset(game.Board.ToBoolArray(40), game.Board.ren, game.Board.b2b);
            };
            
            bot.Reset(game.Board.ToBoolArray(40), game.Board.ren, game.Board.b2b);
        }
        
        public void Update() {
            if (requestMove) {
                var status = bot.PollNextMove(out var move, out _);
                if (status != CCBotPollStatus.Waiting) {
                    requestMove = false;
                }
                
                if (status == CCBotPollStatus.MoveProvided) {
                    lastMove = null;
                    holdRequired = move.hold;
                    firstHold = holdRequired && game.Board.holdPiece == null;
                    if (move.movementCount == 0) {
                        harddrop = true;
                    }
                    foreach (var inst in move.movements.Take(move.movementCount)) {
                        instructions.AddLast(inst);
                    }
                } else if (status == CCBotPollStatus.Dead) {
                    dead = true;
                }
            }
        }

        public Command RequestControlUpdate() {
            if (game.CurrentPiece == null) return 0;

            if (holdRequired) {
                holdRequired = false;
                return Command.Hold;
            }

            if (harddrop) {
                harddrop = false;
                return Command.HardDrop;
            }
            
            if (lastMove == CCMovement.Drop && !game.Board.Collides(game.CurrentPiece.Value.WithOffset(0, -1))) {
                return Command.SoftDrop;
            }
            
            if (instructions.Any()) {
                if (lastMove == instructions.First.Value) {
                    lastMove = null;
                    return 0;
                } else {
                    lastMove = instructions.First.Value;
                    instructions.RemoveFirst();
                    return lastMove switch {
                        CCMovement.Left => Command.Left,
                        CCMovement.Right => Command.Right,
                        CCMovement.Cw => Command.RotateRight,
                        CCMovement.Ccw => Command.RotateLeft,
                        CCMovement.Drop => Command.SoftDrop,
                        _ => throw new ArgumentOutOfRangeException()
                    };
                }
            } else {
                if (lastMove != null) {
                    // end of move
                    lastMove = null;
                    firstHold = false;
                    return Command.HardDrop;
                } else {
                    return 0;
                }
            }
        }

        public void Dispose() {
            bot?.Dispose();
            bot = null;
        }

        ~CcVirtualMachine() {
            Dispose();
        }

        private static CCPiece ConvertCcPiece(PieceKind kind) => kind switch {
            PieceKind.I => CCPiece.I,
            PieceKind.O => CCPiece.O,
            PieceKind.T => CCPiece.T,
            PieceKind.J => CCPiece.J,
            PieceKind.L => CCPiece.L,
            PieceKind.S => CCPiece.S,
            PieceKind.Z => CCPiece.Z
        };
    }
}