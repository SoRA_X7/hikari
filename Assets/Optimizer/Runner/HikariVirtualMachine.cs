using System;
using System.Collections.Generic;
using System.Linq;
using Hikari.AI;
using Hikari.AI.Eval;
using Hikari.AI.Moves;
using Hikari.Puzzle;

namespace Hikari.Optimizer.Runner {
    public class HikariVirtualMachine : IController, IDisposable {
        private HikariAI2 ai;
        private Game game;

        private readonly LinkedList<Instruction> instructions = new LinkedList<Instruction>();
        private bool holdRequired;
        private bool holdOnly;
        private Instruction? lastMove;

        private bool requestMove;
        private bool harddrop;

        public HikariVirtualMachine(Game game, Weights weights) {
            this.game = game;
            ai = new HikariAI2(new HikariConfig {
                previews = 5,
                maxDepth = 24,
                singleThread = false
            }, weights);
            game.Controller = this;
        }

        public void Start() {
            game.OnPieceSpawned += _ => {
                requestMove = true;
            };
            game.OnQueueUpdated += e => {
                ai.AddNextPiece(e.kind);
            };
            
            ai.Reset(game.Board);
        }

        public void Update() {
            ai.Update();

            if (requestMove && IsBoardConsistent()) {
                var mov = ai.GetNextMove();
                if (mov != null) {
                    requestMove = false;
                    lastMove = null;
                    var path = mov.Value.path;
                    // Debug.Log(mov.Value.nodes + " " + path.result);
                    holdRequired = path.hold || path.holdOnly;
                    holdOnly = path.holdOnly;
                    if (!holdOnly && path.instructions.Count == 0) {
                        harddrop = true;
                    }
                    foreach (var inst in path.instructions) {
                        instructions.AddLast(inst);
                    }
                }
            }
        }
        
        private bool IsBoardConsistent() {
            var aiBoard = ai.Board;
            if (game.Board.row.Take(SimpleBoard.Length).Where((row, i) => row.ToBitFlags() != aiBoard.Row(i)).Any()) {
                ai.Reset(game.Board);
                return false;
            }

            return true;
        }

        public Command RequestControlUpdate() {
            if (game.CurrentPiece == null) return 0;

            if (holdRequired) {
                holdRequired = false;
                return Command.Hold;
            }

            if (harddrop) {
                harddrop = false;
                return Command.HardDrop;
            }
            
            if (lastMove == Instruction.SonicDrop && !game.Board.Collides(game.CurrentPiece.Value.WithOffset(0, -1))) {
                return Command.SoftDrop;
            }
            
            if (instructions.Any()) {
                if (lastMove == instructions.First.Value) {
                    lastMove = null;
                    return 0;
                } else {
                    lastMove = instructions.First.Value;
                    instructions.RemoveFirst();
                    return lastMove switch {
                        Instruction.Left => Command.Left,
                        Instruction.Right => Command.Right,
                        Instruction.Cw => Command.RotateRight,
                        Instruction.Ccw => Command.RotateLeft,
                        Instruction.SonicDrop => Command.SoftDrop,
                        _ => throw new ArgumentOutOfRangeException()
                    };
                }
            } else {
                if (lastMove != null && !holdOnly) {
                    // end of move
                    lastMove = null;
                    holdOnly = false;
                    return Command.HardDrop;
                } else {
                    return 0;
                }
            }
        }

        public void Dispose() {
            ai?.Dispose();
        }
    }
}