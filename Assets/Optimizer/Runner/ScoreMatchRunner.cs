using System.Diagnostics;
using Hikari.Puzzle;

namespace Hikari.Optimizer.Runner {
    public class ScoreMatchRunner : IMatchRunner {
        private Match match;
        private Game game;
        private Stopwatch sw;

        public ScoreMatchRunner() {
            match = new Match(1) {
                ImmediateStart = true
            };
            game = match.GetGame(0);
            game.Player = new Game.PlayerInfo {
                Kind = PlayerKind.AI,
                Name = "Hikari"
            };
            sw = Stopwatch.StartNew();
        }

        public void Update() { }

        public void Dispose() { }
    }
}