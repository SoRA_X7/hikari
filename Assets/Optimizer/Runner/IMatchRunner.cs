using System;

namespace Hikari.Optimizer.Runner {
    public interface IMatchRunner : IDisposable {
        void Update();
    }
}