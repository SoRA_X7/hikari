using System;
using System.Diagnostics;
using Hikari.AI.Eval;
using Hikari.Puzzle;
using UnityEngine;

namespace Hikari.Optimizer.Runner {
    public class VsCcMatchRunner : MonoBehaviour {
        private Match match;
        private Game gameHikari;
        private Game gameCc;
        private Stopwatch sw;
        private HikariVirtualMachine hikari;
        private CcVirtualMachine cc;

        public Weights weights = Weights.Default;
        public int iter = 1;
        public int maxBattleSeconds = 60;

        public float reward;
        private int cycle;
        
        public bool Ended { get; private set; }

        private void Start() {
            Ready();
        }

        private void Ready() {
            if (cycle++ >= iter) {
                Ended = true;
                return;
            }
            
            match = new Match(2) {
                ImmediateStart = true
            };
            match.OnFinish += winner => {
                RewardWinOrLose(winner == 0);
                Ready();
            };
            gameHikari = match.GetGame(0);
            gameCc = match.GetGame(1);
            hikari = new HikariVirtualMachine(gameHikari, weights);
            cc = new CcVirtualMachine(gameCc);
            sw = new Stopwatch();

            hikari.Start();
            cc.Start();
            sw.Restart();
        }

        public void Update() {
            hikari.Update();
            cc.Update();

            if (sw.ElapsedMilliseconds > 1000 * maxBattleSeconds) {
                reward -= sw.ElapsedMilliseconds / 1000f;
                RewardStats();
                Ready();
            }
        }

        private void OnDestroy() {
            Dispose();
        }

        private void RewardWinOrLose(bool win) {
            if (win) {
                reward += 500 - sw.ElapsedMilliseconds / 1000f;
            } else {
                reward -= 200 + sw.ElapsedMilliseconds / 1000f;
            }

            if (sw.ElapsedMilliseconds > 3000) {
                RewardStats();
            }
        }

        private void RewardStats() {
            reward += gameHikari.APM;
            reward += gameHikari.PPS;
        }
        
        public void Dispose() {
            cc?.Dispose();
            cc = null;
            hikari?.Dispose();
            hikari = null;
            GC.SuppressFinalize(this);
        }

        ~VsCcMatchRunner() {
            Dispose();
        }
    }
}