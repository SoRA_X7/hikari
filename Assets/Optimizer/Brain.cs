using System;
using System.Collections.Generic;
using System.Linq;
using Hikari.Optimizer.GA;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;

namespace Hikari.Optimizer {
    public class Brain : MonoBehaviour {
        private readonly List<MatchController> matches = new List<MatchController>();
        
        private Population currentPopulation;
        private Queue<Monad> queue = new Queue<Monad>();
        private int playing;
        private int finished;

        [SerializeField] private MatchController matchControllerPrefab;
        [SerializeField] private Transform matchControllerListPanel;

        public int workersNum;

        private bool running;

        private void InstantiateWorkers() {
            for (var i = 0; i < workersNum; i++) {
                var c = Instantiate(matchControllerPrefab, matchControllerListPanel).GetComponent<MatchController>();
                c.id = i + 1;
                matches.Add(c);
            }
        }

        public void BeginTrain() {
            currentPopulation = Population.CreateInitialPopulation(15);
            SetMonads();

            running = true;
        }

        private void Update() {
            if (!running) return;
            
            foreach (var match in matches) {
                if (match.status == MatchControllerStatus.Finished) {
                    // finish process
                    match.ResetToIdle();
                    finished++;
                }

                if (match.status == MatchControllerStatus.Idle && queue.Any()) {
                    match.Init(queue.Dequeue(), playing++);
                }
            }
            
            if (finished == currentPopulation.monads.Count) {
                currentPopulation.Save();
                currentPopulation = currentPopulation.Next();
                SetMonads();
            }
        }

        private void SetMonads() {
            foreach (var monad in currentPopulation.monads) {
                queue.Enqueue(monad);
            }

            playing = 0;
            finished = 0;
            if (matches.Count != workersNum) {
                foreach (var match in matches) {
                    Destroy(match);
                }
                InstantiateWorkers();
            }
        }
    }
}