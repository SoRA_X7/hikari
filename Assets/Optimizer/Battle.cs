namespace Hikari.Optimizer {
    public struct Battle {
        public int monad1;
        public int monad2;

        public Battle(int monad1, int monad2) {
            this.monad1 = monad1;
            this.monad2 = monad2;
        }
    }
}