namespace Hikari.Optimizer {
    public struct OptimizerConfig {
        public MatchMode mode;
        public int matches;
    }
}