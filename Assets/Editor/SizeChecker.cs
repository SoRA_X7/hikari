using System;
using System.Collections.Generic;
using Hikari.AI;
using Hikari.AI.Documents;
using Hikari.AI.Graph;
using Hikari.Puzzle;
using Unity.Collections.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;

namespace Hikari.Editor {
    public static class SizeChecker {
        [MenuItem("Hikari/Check Sizes of Structs")]
        private static void CheckSize() {
            var list = new List<Type> {
                typeof(SimpleBoard),
                typeof(SimpleLockResult),
                typeof(Piece),
                typeof(GraphGroup),
                typeof(GraphNode),
                typeof(GraphChild),
                typeof(GraphChildren),
                typeof(View<ushort>),
                typeof(Selected),
                typeof(Expanded),
                typeof(SpeculationInfo)
            };

            foreach (var type in list) {
                // if (type.IsClass || type.IsInterface) continue;
                var name = type.Name;
                var size = UnsafeUtility.SizeOf(type);
                
                Debug.Log($"{name}: {size}");
            }
        }
    }
}