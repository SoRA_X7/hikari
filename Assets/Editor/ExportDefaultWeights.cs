using System.IO;
using Hikari.AI.Eval;
using MessagePack;
using UnityEditor;
using UnityEngine;

namespace Hikari.Editor {
    public class ExportDefaultWeights {
        [MenuItem("Hikari/Export Weights/MsgPack")]
        public static void ExportMsgPack() {
            var path = "Builds/default.hikari";
            if (string.IsNullOrWhiteSpace(path)) return;
            using var file = File.Create(path);
            MessagePackSerializer.Serialize(file, Weights.Default);
        }
        [MenuItem("Hikari/Export Weights/Json")]
        public static void ExportJson() {
            var path = "Builds/default.json";
            if (string.IsNullOrWhiteSpace(path)) return;
            using var file = File.CreateText(path);
            file.WriteLine(JsonUtility.ToJson(Weights.Default));
        }
    }
}