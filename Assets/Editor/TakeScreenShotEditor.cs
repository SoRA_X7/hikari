using UnityEditor;
using UnityEngine;

namespace Hikari.Editor {
    public static class TakeScreenShotEditor {
        [MenuItem("Hikari/Take Screenshot")]
        private static void TakeScreenShot() {
            ScreenCapture.CaptureScreenshot("ss.png");
            Debug.Log("Screenshot saved");
        }
    }
}