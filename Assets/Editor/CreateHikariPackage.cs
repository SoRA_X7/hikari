using UnityEditor;

namespace Hikari.Editor {
    public static class CreateHikariPackage {
        [MenuItem("Hikari/Export Package")]
        private static void Pack() {
            var assets = new[] {
                "Assets/AI",
                "Assets/Puzzle",
                "Assets/Utils",
                "Assets/Plugins"
            };
            AssetDatabase.ExportPackage(assets, "Builds/Hikari.unitypackage", ExportPackageOptions.Recurse);
        }
    }
}