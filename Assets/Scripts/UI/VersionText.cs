using TMPro;
using UnityEngine;

namespace Hikari.UI {
    public class VersionText : MonoBehaviour {
        private void Start() {
            GetComponent<TMP_Text>().text = "v" + Application.version;
        }
    }
}