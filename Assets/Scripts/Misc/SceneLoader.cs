using Hikari.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hikari.Misc {
    public class SceneLoader : MonoBehaviour {
        private void Start() {
            if (HikariClientSettings.Test1) {
                LoadScene("SelfBattle");
            }
        }

        public void LoadScene(string sceneName) {
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        }
    }
}