using Cysharp.Threading.Tasks;
using Hikari.Puzzle;
using Hikari.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hikari.Misc {
    [RequireComponent(typeof(MatchBehaviour))]
    public class SceneLoadOnMatchEnd : MonoBehaviour {
        private void Start() {
            GetComponent<MatchBehaviour>().match.OnFinish += _ => {
                if (HikariClientSettings.Test1) {
                    Application.Quit();
                } else {
                    UniTask.Run(async () => {
                        await UniTask.Delay(3000);
                        await UniTask.SwitchToMainThread();
                        Debug.Log("Loading Scene");
                        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
                    });
                }
            };
        }
    }
}