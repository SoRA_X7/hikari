using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;

namespace Hikari.AI.UI {
    public class SetSingleThread : MonoBehaviour {
        private void Start() {
            JobsUtility.JobWorkerCount = 1;
        }
    }
}