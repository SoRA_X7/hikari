using TMPro;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UI;

namespace Hikari.AI.UI {
    public class ThreadCountSelector : MonoBehaviour {
        [SerializeField] private Button incrementButton;
        [SerializeField] private Button decrementButton;
        [SerializeField] private TMP_Text workerCount;

        private int minimum;

        private void Start() {
            minimum = FindObjectsOfType<HikariAIBehaviour>().Length;
            JobsUtility.JobWorkerCount = minimum;
            workerCount.text = minimum.ToString();
            decrementButton.interactable = false;
            if (JobsUtility.JobWorkerMaximumCount == 1) incrementButton.interactable = false;
            incrementButton.onClick.AddListener(() => {
                JobsUtility.JobWorkerCount += 1;
                workerCount.text = JobsUtility.JobWorkerCount.ToString();
                decrementButton.interactable = true;
                if (JobsUtility.JobWorkerCount == JobsUtility.JobWorkerMaximumCount) incrementButton.interactable = false;
            });
            decrementButton.onClick.AddListener(() => {
                JobsUtility.JobWorkerCount -= 1;
                workerCount.text = JobsUtility.JobWorkerCount.ToString();
                incrementButton.interactable = true;
                if (JobsUtility.JobWorkerCount == minimum) decrementButton.interactable = false;
            });
        }
    }
}