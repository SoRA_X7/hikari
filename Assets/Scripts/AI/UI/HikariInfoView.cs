using TMPro;
using UnityEngine;

namespace Hikari.AI.UI {
    public class HikariInfoView : MonoBehaviour {
        [SerializeField] private TMP_Text length;
        [SerializeField] private TMP_Text depth;
        [SerializeField] private TMP_Text selectionPerSecond;

        [SerializeField] private Transform moveInfo;
        [SerializeField] private TMP_Text moveNodes;
        [SerializeField] private TMP_Text moveDepth;
        [SerializeField] private TMP_Text moveEval;

        public void Refresh(HikariAI2 ai) {
            length.text = ai.Length.ToString("N0");
            depth.text = ai.MaxDepth.ToString("N0");
            selectionPerSecond.text = ((int) (ai.ParallelCount / Time.fixedDeltaTime)).ToString("N0");
            // selectFail.text = ai.selectFailRate.ToString("P2");
        }

        public void MoveInfo(Move? move) {
            if (move.HasValue) {
                moveInfo.gameObject.SetActive(true);
                moveNodes.text = move.Value.nodes.ToString("N0");
                moveDepth.text = move.Value.depth.ToString("N0");
                moveEval.text = move.Value.eval.ToString();
            } else {
                moveInfo.gameObject.SetActive(false);
            }
        }

        private void Start() {
            moveInfo.gameObject.SetActive(false);
        }
    }
}