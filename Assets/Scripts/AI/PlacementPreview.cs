using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hikari.AI.Visual;
using Hikari.Puzzle;
using UnityEngine;

namespace Hikari.AI {
    public class PlacementPreview : MonoBehaviour {
        private HikariAIBehaviour ai;
        private GameView gameView;
        [SerializeField] private ConnectableCellBlock prefab;
        private readonly List<ConnectableCellBlock> instances = new List<ConnectableCellBlock>();
        private int used;

        private IEnumerator Start() {
            yield return null;
            ai = GetComponentInParent<HikariAIBehaviour>();
            gameView = GetComponentInParent<GameView>();
            ai.OnMovePicked += () => {
                Clear();
                Draw();
            };
        }

        private void Clear() {
            foreach (var block in instances) {
                block.gameObject.SetActive(false);
            }

            used = 0;
        }

        private void Draw() {
            var list = PlacementPreviewFactory.Get(ai.GetPlan(), gameView.game.Board.Clone()).ToList();
            var b2bs = list.Count(d =>
                d.placement.placementKind.IsLineClear() && d.placement.placementKind.IsContinuous());
            var pcs = list.Count(d => d.placement.perfectClear);
            var first = true;
            var opacity = 0.5f;
            foreach (var data in list) {
                for (var i = 0; i < data.cells.Count; i++) {
                    var ins = Rent();
                    ins.Set(data.cells[i].pos.x, data.cells[i].pos.y, first ? 1f : opacity, first ? (PieceKind?) null : data.kind, data.cells[i].con);
                }

                if (data.placement.placementKind.IsLineClear() &&
                    data.placement.placementKind.IsContinuous()) {
                    if (--b2bs <= 0 && pcs <= 0) break;
                    opacity *= 0.5f;
                } else if (data.placement.perfectClear) {
                    if (data.placement.placementKind.IsContinuous()) b2bs--;
                    if (--pcs <= 0 && b2bs <= 0) break;
                    opacity *= 0.5f;
                }

                first = false;
            }
        }

        private ConnectableCellBlock Rent() {
            if (++used >= instances.Count) {
                var obj = Instantiate(prefab, transform, false).GetComponent<ConnectableCellBlock>();
                instances.Add(obj);
                return obj;
            } else {
                instances[used].gameObject.SetActive(true);
                return instances[used];
            }
        }
    }
}