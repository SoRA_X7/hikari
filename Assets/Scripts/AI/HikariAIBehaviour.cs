using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Hikari.AI.Moves;
using Hikari.AI.UI;
using Hikari.AI.Utils.Export;
using Hikari.Puzzle;
using UnityEngine;

namespace Hikari.AI {
    public class HikariAIBehaviour : MonoBehaviour, IController {
        private HikariAI2 ai;
        private Game game;

        private bool requestMove;
        private readonly Queue<Instruction> instructions = new Queue<Instruction>();
        private bool holdRequired;
        private bool holdOnly;
        private bool manipulating;
        private bool waitingForSonicDrop;
        private int hypertap;

        [SerializeField] private bool debugView;
        [SerializeField] private bool logFile;
        [SerializeField] private bool useHold = true;
        [SerializeField] private int previews = 12;

        private HikariInfoView infoView;
        private HikariConfig config;

        private BoardHistoryExporter logger;

        public event Action OnMovePicked;

        private void Awake() {
            config = new HikariConfig {useHold = useHold, previews = previews};
            ai = new HikariAI2(config);
            logger = new BoardHistoryExporter();
        }

        private void Start() {
            infoView = GetComponentInChildren<HikariInfoView>();
            game ??= GetComponent<GameView>().game;

            game.OnInitialized += () => {
                ai.Reset(game.Board);
            };
            game.OnQueueUpdated += e => {
                ai.AddNextPiece(e.kind);
            };
            game.OnPieceSpawned += async _ => {
                await UniTask.Yield();
                requestMove = true;
            };
        }

        private bool IsBoardConsistent() {
            var aiBoard = ai.Board;
            if (game.Board.row.Take(SimpleBoard.Length).Where((row, i) => row.ToBitFlags() != aiBoard.Row(i)).Any()) {
                ai.Reset(game.Board);
                return false;
            }

            return true;
        }

#if UNITY_EDITOR
        private void OnGUI() {
            if (!debugView) return;
            var board = ai.Board;
            for (var y = 0; y < 20; y++) {
                for (var x = 0; x < 10; x++) {
                    if (board.Occupied(x, y)) GUI.Box(new Rect(x * 30, 600 - y * 30, 30, 30), (Texture) null);
                }
            }
        }
#endif

        private void Update() {
            config.previews = previews;
            ai.Update();

            if (infoView) infoView.Refresh(ai);

            if (requestMove && IsBoardConsistent()) {
                var move = ai.GetNextMove();
                if (!move.HasValue) return;

                manipulating = true;
                instructions.Clear();
                var path = move.Value.path;
                if (path.holdOnly) {
                    holdOnly = true;
                    requestMove = false;
                    return;
                }

                foreach (var t in path.instructions) {
                    instructions.Enqueue(t);
                }

                if (path.hold) holdRequired = true;

                if (infoView) infoView.MoveInfo(move);
                
                if (logFile) logger.Append(game.Board);

                OnMovePicked?.Invoke();
                requestMove = false;
            }
        }

        private void OnDestroy() {
            if (logFile) logger.Export();
            ai.Dispose();
        }

        public Command RequestControlUpdate() {
            if (!manipulating) return 0;

            if (waitingForSonicDrop) {
                if (game.IsCurrentPieceGrounded) waitingForSonicDrop = false;
            }

            if (holdOnly) {
                hypertap--;
                holdOnly = false;
                manipulating = false;
                hypertap = 0;
                return Command.Hold;
            }

            Command cmd = 0;

            if (holdRequired) {
                holdRequired = false;
                return Command.Hold;
            }

            if (hypertap++ % 2 != 0) {
                return waitingForSonicDrop ? Command.SoftDrop : 0;
            }


            if (waitingForSonicDrop) {
                cmd |= Command.SoftDrop;
            } else {
                if (instructions.Any()) {
                    switch (instructions.Dequeue()) {
                        case Instruction.Left:
                            cmd |= Command.Left;
                            break;
                        case Instruction.Right:
                            cmd |= Command.Right;
                            break;
                        case Instruction.Cw:
                            cmd |= Command.RotateRight;
                            break;
                        case Instruction.Ccw:
                            cmd |= Command.RotateLeft;
                            break;
                        case Instruction.SonicDrop:
                            waitingForSonicDrop = true;
                            cmd |= Command.SoftDrop;
                            break;
                    }
                } else {
                    cmd |= Command.HardDrop;
                    hypertap = 0;
                    waitingForSonicDrop = false;
                    manipulating = false;
                }
            }

            return cmd;
        }

        public void SetGame(Game game) {
            this.game = game;
        }

        public IEnumerable<Piece> GetPlan() {
            return ai.CurrentPlan;
        }
    }
}