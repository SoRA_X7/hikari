using Hikari.Puzzle;
using Hikari.Puzzle.Visual;
using UnityEngine;

namespace Hikari.AI {
    public class ConnectableCellBlock : MonoBehaviour {
        [SerializeField] private SpriteRenderer down;
        [SerializeField] private SpriteRenderer left;
        [SerializeField] private SpriteRenderer up;
        [SerializeField] private SpriteRenderer right;
        [SerializeField] private SpriteRenderer corner;

        public void Set(int x, int y, float opacity, PieceKind? kind, ConnectionDirection connections) {
            transform.localPosition = new Vector3(x,y,0.5f);

            var color = kind.HasValue ? CellVisualHelper.GetColor(kind.Value) : Color.white;
            color.a = opacity;
            down.color = color;
            left.color = color;
            up.color = color;
            right.color = color;
            corner.color = color;
            
            up.enabled = true;
            right.enabled = true;
            down.enabled = true;
            left.enabled = true;

            if ((connections & ConnectionDirection.Up) != 0) up.enabled = false;
            if ((connections & ConnectionDirection.Right) != 0) right.enabled = false;
            if ((connections & ConnectionDirection.Down) != 0) down.enabled = false;
            if ((connections & ConnectionDirection.Left) != 0) left.enabled = false;
            
        }

        
    }
}