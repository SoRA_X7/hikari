using System;
using UnityEngine;

namespace Hikari.Puzzle.Audio {
    public class GameSoundPlayer : MonoBehaviour {
        [SerializeField] private GameSeList se;

        private AudioSource source;

        private void Start() {
            source = GetComponent<AudioSource>();
        }

        public void PlaySound(SeType sound) {
            var clip = sound switch {
                SeType.Move => se.move,
                SeType.Drop => se.drop,
                SeType.Clear => se.clear,
                SeType.ClearHard => se.clearHard,
                _ => throw new ArgumentOutOfRangeException(nameof(sound))
            };
            source.PlayOneShot(clip);
        }
    }
}