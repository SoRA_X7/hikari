namespace Hikari.Puzzle.Audio {
    public enum SeType {
        Move,
        Drop,
        Clear,
        ClearHard
    }
}