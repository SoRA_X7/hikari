using UnityEngine;

namespace Hikari.Puzzle.Audio {
    [CreateAssetMenu(fileName = "Game SE", menuName = "Hikari/Game SE List", order = 0)]
    public class GameSeList : ScriptableObject {
        public AudioClip move;
        public AudioClip drop;
        public AudioClip clear;
        public AudioClip clearHard;
    }
}