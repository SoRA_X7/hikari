using TMPro;
using UnityEngine;

namespace Hikari.Puzzle.UI {
    [DefaultExecutionOrder(200)]
    public class GameInfoUI : MonoBehaviour {
        [SerializeField] private TMP_Text apm;
        [SerializeField] private TMP_Text lpm;
        [SerializeField] private TMP_Text pps;

        private GameView gv;

        private void Start() {
            gv = GetComponentInParent<GameView>();
        }

        private void Update() {
            apm.text = gv.game.APM.ToString("N1");
            lpm.text = gv.game.LPM.ToString("N1");
            pps.text = gv.game.PPS.ToString("N1");
        }
    }
}