using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Hikari.Puzzle.UI {
    public class AllClearVisualUI : MonoBehaviour {
        [SerializeField] private Image line0;
        [SerializeField] private Image line1;
        [SerializeField] private TMP_Text text;

        private void Start() {
            var seq = DOTween.Sequence();
            seq.Append(line0.DOFade(0, .3f).From())
                .Join(line1.DOFade(0, .3f).From())
                .Insert(.05f, text.DOFade(0, .7f).From())
                // .Join(text.transform.DOMoveX(-10, .7f).From().SetRelative(true))
                // .Insert(1.5f, text.transform.DOMoveX(10, .7f).SetRelative(true))
                .Join(DOTween.To(() => text.characterSpacing, value => text.characterSpacing = value, 50, 3f))
                .Insert(1.5f, text.DOFade(0, .5f))
                .Join(line0.DOFade(0, .5f))
                .Join(line1.DOFade(0, .5f))
                .AppendCallback(() => Destroy(gameObject));
        }
    }
}