using System.Linq;
using TMPro;
using UnityEngine;

namespace Hikari.Puzzle.UI {
    public class CameraModeSelector : MonoBehaviour {
        private Camera camera;
        [SerializeField] private TMP_Dropdown dropdown;

        private const string CameraSettingKey = "Hikari/camera_mode";

        private void Start() {
            camera = Camera.main;
            dropdown.onValueChanged.AddListener(OnDropdownChange);

            if (PlayerPrefs.HasKey(CameraSettingKey)) dropdown.value = PlayerPrefs.GetInt(CameraSettingKey);
        }

        private void OnDropdownChange(int value) {
            var games = FindObjectsOfType<GameView>();
            if (!games.Any()) return;

            if (games.Length <= 2) {
                camera.fieldOfView = 50;
                camera.orthographicSize = 13;
                camera.orthographic = value == 1;
                foreach (var tr in games.Select(g => g.transform)) {
                    if (value == 1) tr.localRotation = Quaternion.identity;
                    else {
                        if (tr.position.x > 5) {
                            tr.localRotation = Quaternion.Euler(0,5,0);
                        } else if (tr.position.x < -5) {
                            tr.localRotation = Quaternion.Euler(0,-5,0);
                        } else {
                            tr.localRotation = Quaternion.identity;
                        }
                    }
                }
            }

            PlayerPrefs.SetInt(CameraSettingKey, value);
            PlayerPrefs.Save();
        }
    }
}