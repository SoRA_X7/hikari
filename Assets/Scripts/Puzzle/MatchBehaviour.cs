using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.LowLevel;

namespace Hikari.Puzzle {
    public class MatchBehaviour : MonoBehaviour {
        public Match match;

        [SerializeField] private List<TMP_Text> countdownTexts;
        [SerializeField] private bool immediateStart;
        [SerializeField] private int gameCount;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void InitializePlayerLoop() {
            var playerLoop = PlayerLoop.GetCurrentPlayerLoop();
            PlayerLoopHelper.Initialize(ref playerLoop);
        }

        private void Awake() {
            match = new Match(gameCount);
        }

        private void Start() {
            countdownTexts.ForEach(t => t.text = "");
            match.OnCountDown += c => {
                if (c > 0) {
                    countdownTexts.ForEach(t => t.text = c.ToString());
                } else if (c == 0) {
                    countdownTexts.ForEach(t => t.text = "GO");
                    UniTask.Run(async () => {
                        await UniTask.Delay(1000);
                        await UniTask.SwitchToMainThread();
                        countdownTexts.ForEach(t => t.text = "");
                    }).Forget();
                }
            };
        }

        private void Update() {
            match.Update();
        }
    }
}