using DG.Tweening;
using UnityEngine;

namespace Hikari.Puzzle {
    public class DamageBar : MonoBehaviour {
        private Tween tween;
        private uint amount;

        private void Start() {
            transform.localScale = new Vector3(1, 0, 1);
        }

        public uint Amount {
            set {
                if (value == amount) return;
                tween?.Complete();
                if (value == 0) {
                    tween = transform.DOScaleY(value, 0.2f).SetEase(Ease.OutQuint)
                        .OnComplete(() => gameObject.SetActive(false));
                } else {
                    gameObject.SetActive(true);
                    tween = transform.DOScaleY(value, 0.2f).SetEase(Ease.OutQuint);
                }

                amount = value;
            }
        }
    }
}