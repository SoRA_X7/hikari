using System.Linq;
using Cysharp.Threading.Tasks;
using Hikari.Puzzle.Audio;
using Hikari.Puzzle.UI;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.VFX;

namespace Hikari.Puzzle {
    public class GameView : MonoBehaviour {
        public Game game;

        [FormerlySerializedAs("playerIndex")] [SerializeField]
        private int index;

        [SerializeField] private Transform fieldOrigin;
        [SerializeField] private FallingPieceBehaviour[] queue;
        [SerializeField] private FallingPieceBehaviour holdPiece;
        [SerializeField] private FallingPieceBehaviour controllingPiece;
        [SerializeField] private FallingPieceBehaviour ghostPiece;
        [SerializeField] private DamageBar damageBar;
        [SerializeField] private TMP_Text playerNameText;
        [SerializeField] private CellBlock cellPrefab;
        [SerializeField] private VisualEffect lineClearEffect;
        [SerializeField] private AttackSummaryUI attackSummary;
        [SerializeField] private GameSoundPlayer soundPlayer;
        [SerializeField] private RenVisionUI renVisionPrefab;
        [SerializeField] private B2BVisionUI b2bVisionPrefab;
        [SerializeField] private AllClearVisualUI allClearVisionPrefab;
        private Transform canvas;

        private readonly CellBlock[][] cells = new CellBlock[40][];

        [Header("Settings")] public bool showGhost = true;

        [Header("Player")] [SerializeField] private string playerName;
        [SerializeField] private PlayerKind playerKind;

        private bool isInClearEffect;

        private void Awake() {
            for (var i = 0; i < cells.Length; i++) {
                cells[i] = new CellBlock[10];
            }

            controllingPiece.gameObject.SetActive(false);
        }

        private void Start() {
            var mb = FindObjectOfType<MatchBehaviour>();
            if (!mb) {
                Debug.LogError("MatchBehaviour is not found in scene");
            }

            game = mb.match.GetGame(index);
            game.Player = new Game.PlayerInfo {
                Kind = playerKind,
                Name = playerName
            };

            Init();
        }

        private void Update() {
            if (game == null) return;
            if (!isInClearEffect) damageBar.Amount = game.Damage;
        }

        private void Init() {
            canvas = attackSummary.transform.parent;
            damageBar.Amount = 0;
            playerNameText.text = game.Player.Name;
            var controller = GetComponent<IController>();
            if (controller != null) game.Controller = controller;
            game.OnQueueUpdated += _ => {
                for (var i = 0; i < queue.Length; i++) {
                    queue[i].piece = new Piece(game.Board.nextPieces.ElementAt(i), 0, 0, 0);
                    queue[i].MakeShapeAndColor();
                }

                UpdateCurrentPiece();
            };
            game.OnFallingPieceMoved += e => {
                // soundPlayer.PlaySound(SeType.Move);
                UpdateCurrentPiece();
            };
            game.OnGarbageLinesAdded += OnGarbageLinesReceived;
            game.OnPieceLocked += OnPieceLocked;
            game.OnHold += e => {
                if (game.Board.holdPiece == null) {
                    holdPiece.gameObject.SetActive(false);
                } else {
                    holdPiece.gameObject.SetActive(true);
                    holdPiece.piece = new Piece(game.Board.holdPiece.Value, 0, 0, 0);
                    holdPiece.MakeShapeAndColor();
                    UpdateCurrentPiece();
                }
            };
        }

        private async void OnPieceLocked(Game.PieceLockedEvent e) {
            foreach (var position in e.piece.GetCellPositions()) {
                if (cells[position.y][position.x] != null) continue;
                var obj = Instantiate(cellPrefab.gameObject, fieldOrigin);
                var block = obj.GetComponent<CellBlock>();
                block.transform.localPosition = new Vector3(position.x, position.y);
                block.materialIndex = (int)e.piece.Kind;
                block.UpdateMaterial();
                cells[position.y][position.x] = block;
            }

            controllingPiece.gameObject.SetActive(false);
            ghostPiece.gameObject.SetActive(false);

            soundPlayer.PlaySound(SeType.Drop);

            if (!e.lockResult.clearedLines.Any()) {
                return;
            }

            isInClearEffect = true;
            attackSummary.Show(e.lockResult.placementKind);
            if (e.lockResult.ren > 1) {
                Instantiate(renVisionPrefab, canvas).GetComponent<RenVisionUI>().renCount = (int)(e.lockResult.ren - 1);
            }

            if (e.lockResult.b2b && e.prevB2B) {
                Instantiate(b2bVisionPrefab, canvas);
            }

            if (e.lockResult.perfectClear) {
                Instantiate(allClearVisionPrefab, canvas);
            }

            if (e.lockResult.placementKind.IsContinuous()) {
                soundPlayer.PlaySound(SeType.ClearHard);
            } else {
                soundPlayer.PlaySound(SeType.Clear);
            }

            foreach (var line in e.lockResult.clearedLines) {
                var obj = Instantiate(lineClearEffect.gameObject, fieldOrigin);
                obj.transform.localPosition = new Vector3(4.5f, line, -0.5f);
                var vfx = obj.GetComponent<VisualEffect>();
                vfx.SetInt("Piece Color", (int)e.piece.Kind);
            }

            if (!e.lockResult.perfectClear) await UniTask.DelayFrame(18);

            foreach (var line in e.lockResult.clearedLines) {
                for (var i = 0; i < cells[line].Length; i++) {
                    var cellBlock = cells[line][i];
                    if (cellBlock && cellBlock.gameObject) Destroy(cellBlock.gameObject);
                    cells[line][i] = null;
                }
            }

            if (!e.lockResult.perfectClear) await UniTask.DelayFrame(20);

            var c = 0;
            foreach (var line in e.lockResult.clearedLines) {
                for (var i = line - c; i < cells.Length - 1; i++) {
                    cells[i] = cells[i + 1];
                }

                c++;
            }

            for (var i = 0; i < e.lockResult.clearedLines.Count; i++) {
                cells[cells.Length - 1 - i] = new CellBlock[10];
            }

            for (var y = 0; y < cells.Length; y++) {
                for (var x = 0; x < 10; x++) {
                    if (cells[y][x]) cells[y][x].transform.localPosition = new Vector3(x, y);
                }
            }

            // damageBar.Amount = 0;
            isInClearEffect = false;
        }

        private async void OnGarbageLinesReceived(Game.GarbageLinesAddedEvent e) {
            if (isInClearEffect) await UniTask.DelayFrame(40);

            damageBar.Amount = 0;

            var move = e.rows.Length;
            for (var i = cells.Length - 1; i >= cells.Length - e.rows.Length; i--) {
                foreach (var cellBlock in cells[i].Where(cb => cb)) {
                    Destroy(cellBlock.gameObject);
                }
            }

            for (var i = cells.Length - e.rows.Length - 1; i >= 0; i--) {
                cells[i + e.rows.Length] = cells[i];
                foreach (var cellBlock in cells[i + e.rows.Length].Where(cb => cb)) {
                    cellBlock.transform.Translate(new Vector3(0, move, 0), Space.Self);
                }
            }

            for (var i = 0; i < e.rows.Length; i++) {
                cells[i] = new CellBlock[10];
                for (var j = 0; j < 10; j++) {
                    var cellValue = e.rows[i][j];
                    if (cellValue != 0) {
                        var obj = Instantiate(cellPrefab.gameObject, fieldOrigin);
                        obj.transform.localPosition = new Vector3(j, i);
                        var block = obj.GetComponent<CellBlock>();
                        block.materialIndex = cellValue;
                        cells[i][j] = block;
                    }
                }
            }
        }

        private void UpdateCurrentPiece() {
            if (game.CurrentPiece.HasValue) {
                controllingPiece.gameObject.SetActive(true);
                controllingPiece.piece = game.CurrentPiece.Value;
                controllingPiece.MakeShapeAndColor();
                controllingPiece.UpdatePosition();
            } else {
                controllingPiece.gameObject.SetActive(false);
            }

            if (showGhost && game.Ghost.HasValue) {
                ghostPiece.gameObject.SetActive(true);
                ghostPiece.piece = game.Ghost.Value;
                ghostPiece.MakeShapeAndColor();
                ghostPiece.UpdatePosition();
            } else {
                ghostPiece.gameObject.SetActive(false);
            }
        }
    }
}