using System.Threading;
using Unity.Jobs;
using UnityEngine;

namespace Hikari.Prototype
{
    public class JobCompleteTest1 : MonoBehaviour {
        private JobHandle jobHandle;
        // Start is called before the first frame update
        void Start()
        {
            jobHandle = new TestJob1().Schedule();
        }

        // Update is called once per frame
        void Update() {
            Debug.Log(jobHandle.IsCompleted.ToString());
        }
    }

    public struct TestJob1 : IJob {
        public void Execute() {
            Debug.Log("RUN");
            Thread.Sleep(5000);
            Debug.Log("END");
        }
    }
}
