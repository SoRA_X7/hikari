using Hikari.Puzzle;
using Unity.Collections;
using Unity.Mathematics;

namespace Hikari.AI.Moves {
    public static class PathFinder {
        public static Path? FindPath(in SimpleBoard board, Piece piece, bool useHold,
            in NativeArray<uint4x4> pieceShapes, in NativeArray<int2x4> pieceCells) {
            var spawned = board.Spawn(piece.Kind, pieceShapes);
            if (!spawned.HasValue) return null;

            using var mirai = new Mirai(board, pieceShapes, pieceCells);
            mirai.Generate(spawned.Value);

            return mirai.RebuildPath(piece, useHold);
        }
    }
}