using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Hikari.AI.Jobs {
    [BurstCompile(OptimizeFor = OptimizeFor.Performance)]
    public struct NativeListDistinctJob<T> : IJob where T : unmanaged, IEquatable<T>, IComparable<T> {
        public NativeArray<T> list;
        public NativeReference<int> lengthOutput;
        
        public void Execute() {
            list.Sort();
            lengthOutput.Value = list.Unique();
        }
    }
}