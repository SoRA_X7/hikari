using System;
using System.Collections.Generic;
using System.Linq;
using Hikari.AI.Moves;
using Hikari.Puzzle;
using NUnit.Framework;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;

namespace Hikari.AI.Tests {
    public class MiraiTests {
        private class TestObj : IDisposable {
            public NativeArray<uint4x4> pieceShapes;
            public NativeArray<int2x4> pieceCells;
            public Mirai mirai;

            public TestObj() {
                pieceShapes = new NativeArray<uint4x4>(Piece.NativeShapes, Allocator.Persistent);
                pieceCells = new NativeArray<int2x4>(Piece.Cells, Allocator.Persistent);
            }

            public void Init(SimpleBoard board) {
                mirai = new Mirai(board, pieceShapes, pieceCells);
            }

            public void Dispose() {
                mirai.Dispose();
                pieceShapes.Dispose();
                pieceCells.Dispose();
            }
        }

        private class Expected {
            private readonly string ex;

            public Expected(string expects) {
                ex = string.Join('\n', expects.Trim().Split('\n').Select(line => {
                        var splits = line.Split(' ');
                        var cost = splits.Last();
                        return (line[..^cost.Length].Trim(), int.Parse(cost));
                    })
                    .OrderBy(tuple => tuple.Item2)
                    .ThenBy(tuple => tuple.Item1)
                    .Select(tuple => $"{tuple.Item1} {tuple.Item2}"));
                Debug.Log("Expects \n" + ex);
            }
            
            public void Compare(UnsafeHashMap<Piece, StepRef> actual) {
                using var keys = actual.GetKeyArray(Allocator.Temp);
                var str = string.Join('\n', keys.OrderBy(k => actual[k].cost)
                    .ThenBy(k => k.ToString())
                    .Select(k => $"{k} {actual[k].cost}"));
                Debug.Log("Got \n" + str);
                Assert.AreEqual(ex, str);
            }
        }

        [Test]
        public void GenerateEmptyO() {
            var expected = new Expected(@"
O (3,-1) 0 0
O (2,-1) 0 1
O (4,-1) 0 1
O (1,-1) 0 3
O (5,-1) 0 3
O (0,-1) 0 5
O (6,-1) 0 5
O (-1,-1) 0 7
O (7,-1) 0 7
");
            using var ob = new TestObj();
            var board = new SimpleBoard();
            ob.Init(board);
            ob.mirai.Generate(new Piece(PieceKind.O));
            expected.Compare(ob.mirai.locked);
        }

        [Test]
        public void GenerateDeadMoves() {
            using var ob = new TestObj();
            var board = new SimpleBoard();
            board.LockSelf(new Piece(PieceKind.I, 1, 17, 0), ob.pieceShapes);
            board.LockSelf(new Piece(PieceKind.I, 5, 17, 0), ob.pieceShapes);
            ob.Init(board);
            ob.mirai.Generate(new Piece(PieceKind.O, 3, 19, 0));
            Assert.AreEqual(9, ob.mirai.locked.Count());
            foreach (var kv in ob.mirai.locked) {
                var clone = board;
                var lr = clone.LockSelf(kv.Key, ob.pieceShapes);
                Assert.IsTrue(lr.death);
            }
        }
    }
}