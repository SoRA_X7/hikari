using System;
using System.Runtime.InteropServices;
using Hikari.Puzzle;
using Unity.Burst.CompilerServices;
using Unity.Mathematics;

namespace Hikari.AI.Graph {
    [StructLayout(LayoutKind.Explicit)]
    public struct SpeculationInfo : IEquatable<SpeculationInfo> {
        [FieldOffset(0)] private byte I;
        [FieldOffset(1)] private byte O;
        [FieldOffset(2)] private byte T;
        [FieldOffset(3)] private byte J;
        [FieldOffset(4)] private byte L;
        [FieldOffset(5)] private byte S;
        [FieldOffset(6)] private byte Z;
        [FieldOffset(7)] private byte possibilities;

        [FieldOffset(0)] private ulong all;

        public readonly int Sum => I + O + T + J + L + S + Z;

        public readonly bool IsKnown() => math.countbits((uint)possibilities) == 1;
        public readonly PieceKind GetKnownPiece() => (PieceKind)math.tzcnt((uint)possibilities);

        public readonly bool Has(int kind) {
            return (possibilities & (1U << kind)) != 0U;
        }

        public readonly bool Has(PieceKind kind) => Has((int)kind);

        public readonly int CountPossiblePieces() => math.countbits((uint)possibilities);

        public readonly unsafe byte GetLengthOf([AssumeRange(0, 6)] int kind) {
            fixed (SpeculationInfo* ptr = &this) {
                return ((byte*)ptr)[kind];
            }
        }

        public readonly byte GetLengthOf(PieceKind kind) => GetLengthOf((int)kind);

        public readonly int GetStartOf([AssumeRange(0, 6)] int kind) {
            var start = 0;
            for (var i = 0; i < kind; i++) {
                start += GetLengthOf(i);
            }

            return start;
        }

        public readonly int GetStartOf(PieceKind kind) => GetStartOf((int)kind);

        public unsafe void Set(PieceKind kind, byte length) {
            fixed (SpeculationInfo* ptr = &this) {
                ((byte*)ptr)[(int)kind] = length;
            }

            possibilities |= (byte)(1 << (int)kind);
        }

        public bool Resolve(PieceKind resolved) {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            if (possibilities == 0) {
                throw new Exception("Speculation is invalid");
            }
#endif

            if (!Has(resolved)) {
                return false;
            }

            possibilities = (byte)(1 << (int)resolved);
            return true;
        }

        public bool Equals(SpeculationInfo other) {
            return all == other.all;
        }

        public override bool Equals(object obj) {
            return obj is SpeculationInfo other && Equals(other);
        }

        public override int GetHashCode() {
            return all.GetHashCode();
        }

        public static bool operator ==(SpeculationInfo left, SpeculationInfo right) {
            return left.Equals(right);
        }

        public static bool operator !=(SpeculationInfo left, SpeculationInfo right) {
            return !left.Equals(right);
        }
    }
}
