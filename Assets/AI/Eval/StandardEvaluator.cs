using System.Runtime.CompilerServices;
using Hikari.Puzzle;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Hikari.AI.Eval {
    public unsafe struct StandardEvaluator : IEvaluator {
        public Value EvaluateBoard([NoAlias] in SimpleBoard board, [NoAlias] in Weights w,
            [NoAlias] in NativeArray<uint4x4> pieceShapes) {
            var clone = board;
            var fieldSafety = 0;
            var fieldPower = 0;
            var columns = stackalloc int[10];
            var maxHeights = stackalloc byte[10];
            clone.GetColumns(columns, maxHeights);
            var maxHeight = CalcMaxHeight(clone);

            if (!w.noTspin) {
                var tPieceCount = math.select(0, 1, clone.bag.Contains(PieceKind.T)) +
                                  math.select(0, 1, clone.hold == PieceKind.T) +
                                  math.select(0, 1, clone.bag.Count <= 3);
                for (var i = 0; i < tPieceCount; i++) {
                    var tHole = ScanTspin(clone, maxHeights, maxHeight);
                    if (tHole != null) {
                        fieldPower += tHole.Value.lines * w.tHole;
                        AfterSpin(ref clone, tHole.Value, pieceShapes);
                        clone.GetColumns(columns, maxHeights);
                        maxHeight -= tHole.Value.lines;
                        continue;
                    }

                    var tst = ScanTST(clone, maxHeights, maxHeight);
                    if (tst != null) {
                        fieldPower += tst.Value.lines * w.tstHole;
                        AfterSpin(ref clone, tst.Value, pieceShapes);
                        clone.GetColumns(columns, maxHeights);
                        maxHeight -= tst.Value.lines;
                        continue;
                    }

                    break;
                }
            }

            // board.GetColumns(columns, maxHeights);

            var holes = CalcHolePos(clone, ref maxHeights);
            var holeColumn = holes.x;
            var depth = holes.y;

            fieldSafety += math.csum(CalcBumpiness(ref maxHeights, holeColumn) * new int2(w.bumpSum, w.bumpSumSq));

            fieldPower += depth * w.holeDepth;
            if (depth >= 2) {
                fieldPower += w.wellX[holeColumn];
            }

            var maxDiff = 0;
            for (var x = 0; x < 8; x++) {
                if (x == holeColumn || x + 1 == holeColumn) continue;
                maxDiff = math.max(maxDiff, math.abs(maxHeights[x + 1] - maxHeights[x]));
            }

            fieldSafety += maxDiff * w.maxHeightDiff;

            fieldSafety += maxHeight * w.maxHeight;
            fieldSafety += math.max(maxHeight - 10, 0) * w.top50;
            fieldSafety += math.max(maxHeight - 15, 0) * w.top75;


            var cavOvh = CavitiesAndOverhangs(clone, columns, maxHeight);
            fieldSafety += cavOvh.x * w.cavities;
            fieldSafety += cavOvh.x * cavOvh.x * w.cavitiesSq;
            fieldSafety += cavOvh.y * w.overhangs;
            fieldSafety += cavOvh.y * cavOvh.y * w.overhangsSq;

            fieldSafety += math.csum(CoveredCells(clone, maxHeights) * new int2(w.coveredCells, w.coveredCellsSq));

            var trans = 0;
            for (var y = 0; y < SimpleBoard.Length; y++) {
                trans += math.countbits((clone.cells[y] | (1 << 10)) ^ (1 | (clone.cells[y] << 1)));
            }

            fieldSafety += trans * w.rowTransitions;

            fieldPower += math.floorlog2(clone.ren) * w.downstack;

            return new Value {
                value = new int4(fieldSafety, fieldPower, 0, 0),
                spike = 0
            };
        }

        public Reward EvaluateMove(int time, Piece piece,
            [NoAlias] in SimpleBoard board, [NoAlias] in SimpleLockResult lr, bool parentIsB2B,
            [NoAlias] in Weights w) {
            var moveScore = 0;
            if (!piece.IsInvalid) {
                if (!lr.perfectClear && lr.placementKind.IsLineClear()) time += 40;
                moveScore += time * w.moveTime;

                var maxDangerHeight = CalcMaxHeight(board, 0b0001111000);
                moveScore += math.max(maxDangerHeight - 15, 0) * time * w.danger;

                if (lr.placementKind.IsLineClear() && lr.backToBack) moveScore += w.b2bContinue;
                if (parentIsB2B && !lr.backToBack) moveScore += w.b2bDestroy;

                moveScore += piece.Y * w.placementHeight;

                if (lr.perfectClear) {
                    moveScore += w.perfect;
                } else {
                    var renAttack = Game.GetRenAttack(lr.ren - 1);
                    moveScore += w.ren * renAttack * renAttack;
                    moveScore += lr.placementKind switch {
                        PlacementKind.Clear1 => w.clear1,
                        PlacementKind.Clear2 => w.clear2,
                        PlacementKind.Clear3 => w.clear3,
                        PlacementKind.Clear4 => w.clear4,
                        PlacementKind.Mini1 => w.tMini1,
                        PlacementKind.Mini2 => w.tMini2,
                        PlacementKind.TSpin1 => w.tSpin1,
                        PlacementKind.TSpin2 => w.tSpin2,
                        PlacementKind.TSpin3 => w.tSpin3,
                        _ => 0
                    };
                }

                if (piece.Kind == PieceKind.T) {
                    if (!lr.placementKind.IsFullTspinClear()) {
                        moveScore += w.wastedT;
                    }
                }
            }

            if (board.hold == PieceKind.T && !lr.placementKind.IsContinuous()) {
                moveScore += w.holdT;
            }

            return new Reward {
                evaluation = moveScore,
                attack = lr.GetAttack()
            };
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int2 CoveredCells([NoAlias] in SimpleBoard board, [NoAlias] byte* maxHeights) {
            var covered = 0;
            var sq = 0;

            for (var x = 0; x < 10; x++) {
                for (var y = maxHeights[x] - 2 - 1; y >= 0; y--) {
                    if (board.OccupiedUnbounded(x, y)) continue;
                    var cells = maxHeights[x] - y - 1;
                    covered += cells;
                    sq += cells * cells;
                }
            }

            return new int2(covered, sq);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int2 CalcHolePos(in SimpleBoard board, ref byte* cMaxHeights) {
            var hole = 0;
            for (var x = 1; x < 10; x++) {
                if (cMaxHeights[x] <= cMaxHeights[hole]) {
                    hole = x;
                }
            }

            var depth = 0;
            for (var y = cMaxHeights[hole]; y < SimpleBoard.Length; y++) {
                if (math.countbits((uint) board.cells[y]) == 9 && !board.OccupiedUnbounded(hole, y)) {
                    depth += 1;
                } else {
                    break;
                }
            }

            return new int2(hole, depth);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int2 CalcBumpiness(ref byte* cMaxHeights, int well) {
            var bumpiness = -1;
            var bumpinessSq = -1;

            var prev = 0;

            for (var x = 0; x < 10; x++) {
                if (x == well) continue;
                if (prev >= 0) {
                    var dh = math.abs(prev - cMaxHeights[x]);
                    bumpiness += dh;
                    bumpinessSq += dh * dh;
                }

                prev = cMaxHeights[x];
            }

            return new int2(math.abs(bumpiness), math.abs(bumpinessSq));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int CalcMaxHeight(in SimpleBoard board) {
            var max = 0;
            for (var i = 0; i < SimpleBoard.Length; i++) {
                if (board.cells[i] != 0) max = i;
            }

            return max;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int CalcMaxHeight(in SimpleBoard board, int filter) {
            var max = 0;
            for (var i = 0; i < SimpleBoard.Length; i++) {
                if ((board.cells[i] & filter) != 0) max = i;
            }

            return max;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int2 CavitiesAndOverhangs([NoAlias] in SimpleBoard board, [NoAlias] int* colHeights,
            int maxHeight) {
            var cavities = 0;
            var overhangs = 0;
            for (var y = 0; y < maxHeight; y++) {
                for (var x = 0; x < 10; x++) {
                    if (y >= colHeights[x]) continue;
                    if (board.OccupiedUnbounded(x, y)) continue;

                    if (x >= 2) {
                        if (colHeights[x - 1] <= y - 1 && colHeights[x - 2] <= y) {
                            overhangs++;
                            continue;
                        }
                    }

                    if (x < 8) {
                        if (colHeights[x + 1] <= y - 1 && colHeights[x + 2] <= y) {
                            overhangs++;
                            continue;
                        }
                    }

                    cavities++;
                }
            }

            return new int2(cavities, overhangs);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static TspinHole? ScanTspin(in SimpleBoard board, byte* maxHeights, int maxHeight) {
            static void Check(in SimpleBoard b, int x, int y, ref TspinHole? spinHole) {
                var lines = 0;
                if (math.countbits((uint) b.cells[y]) == 9) lines++;
                if (math.countbits((uint) b.cells[y + 1]) == 7) lines++;

                if (lines == 0) return;

                if (spinHole == null || spinHole.Value.lines < lines) {
                    spinHole = new TspinHole(lines, x, y, 2);
                }
            }

            var tShape = new uint4(0b010, 0b111, 0, 0);

            TspinHole? best = null;
            for (var y = maxHeight - 3; y >= 0; y--) {
                for (var x = 0; x < 8; x++) {
                    var pos = new int2(x, y);
                    if (board.Collides(tShape, pos)) continue;
                    if (!board.Occupied(x, y) || !board.Occupied(x + 2, y)) continue;

                    if (board.Occupied(x, y + 2)) {
                        if (maxHeights[x + 1] > y + 1 || maxHeights[x + 2] > y + 1) continue;
                        Check(board, x, y, ref best);
                    } else if (board.Occupied(x + 2, y + 2)) {
                        if (maxHeights[x + 1] > y + 1 || maxHeights[x] > y + 1) continue;
                        Check(board, x, y, ref best);
                    }
                }
            }

            return best;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static TspinHole? ScanTST([NoAlias] in SimpleBoard board, [NoAlias] byte* maxHeights, int maxHeight) {
            static void Check(in SimpleBoard b, int x, int y, int r, ref TspinHole? spinHole) {
                var filled = 0;
                if (b.Occupied(x, y)) filled++;
                if (b.Occupied(x + 2, y)) filled++;
                if (b.Occupied(x + 2, y + 2)) filled++;
                if (b.Occupied(x, y + 2)) filled++;
                if (filled < 3) return;

                var lines = 0;
                if (math.countbits((uint) b.cells[y]) == 9) lines++;
                if (math.countbits((uint) b.cells[y + 1]) == 8) lines++;
                if (math.countbits((uint) b.cells[y + 2]) == 9) lines++;

                if (lines == 0) return;

                if (spinHole == null || spinHole.Value.lines < lines) {
                    spinHole = new TspinHole(lines, x, y, r);
                }
            }

            // []    {}
            // ......{}
            // ..[]
            // ....
            // ..
            var slotL = new uint4(0b010, 0b110, 0b010, 0b110);
            // {}    []
            // {}......
            //     []..    
            //     ....
            //       ..
            var slotR = new uint4(0b010, 0b011, 0b010, 0b011);

            // scan for tst shape
            TspinHole? best = null;
            for (var y = maxHeight - 4 - 1; y >= 0; y--) {
                for (var x = -1; x < 8; x++) {
                    if (!board.Collides(slotL, new int2(x, y)) &&
                        !board.Occupied(x + 3, y + 3) &&
                        board.Occupied(x + 1, y + 4) &&
                        board.Occupied(x + 2, y + 2) &&
                        (board.Occupied(x + 2, y) || board.Occupied(x + 1, y - 1)) &&
                        board.Occupied(x + 4, y + 3) == board.Occupied(x + 4, y + 4)) {
                        // sky above?
                        if (x + 3 < 10 && (maxHeights[x + 2] > y + 2 || maxHeights[x + 3] > y + 2)) continue;
                        if (x + 4 < 10 && !board.Occupied(x + 4, y + 3) && maxHeights[x + 4] > y + 2) continue;
                        Check(in board, x, y, 1, ref best);
                    } else if (!board.Collides(slotR, new int2(x, y)) &&
                               !board.Occupied(x - 1, y + 3) &&
                               board.Occupied(x + 1, y + 4) &&
                               board.Occupied(x + 0, y + 2) &&
                               (board.Occupied(x, y) || board.Occupied(x + 1, y - 1)) &&
                               board.Occupied(x - 2, y + 3) == board.Occupied(x - 2, y + 4)) {
                        // sky above?
                        if (x - 1 >= 0 && (maxHeights[x] > y + 2 || maxHeights[x - 1] > y + 2)) continue;
                        if (x - 2 >= 0 && !board.Occupied(x - 2, y + 3) && maxHeights[x - 2] > y + 2) continue;
                        Check(in board, x, y, 3, ref best);
                    }
                }
            }

            return best;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void AfterSpin(ref SimpleBoard board, TspinHole ts, in NativeArray<uint4x4> pieceShapes) {
            var piece = new Piece(PieceKind.T, (sbyte) ts.x, (sbyte) ts.y, (sbyte) ts.r, TSpinStatus.Full);
            // arbitrary place T
            board.LockSelf(piece, pieceShapes);
        }
    }
}