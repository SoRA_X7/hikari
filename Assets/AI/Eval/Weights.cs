using System;
#if HIKARI_CLIENT
using MessagePack;
#endif

namespace Hikari.AI.Eval {
#if HIKARI_CLIENT
    [MessagePackObject(keyAsPropertyName: true)]
#endif
    [Serializable]
    public struct Weights {
        public int clear1;
        public int clear2;
        public int clear3;
        public int clear4;
        public int tSpin1;
        public int tSpin2;
        public int tSpin3;
        public int tMini1;
        public int tMini2;

        public int perfect;

        public int tHole;
        public int tstHole;
        public int finHole;

        public int b2bContinue;
        public int b2bDestroy;
        public int ren;
        
        public int wastedT;
        public int holdT;

        public int bumpSum;
        public int bumpSumSq;
        public int maxHeightDiff;
        public int holeDepth;
        
        public int maxHeight;
        public int top50;
        public int top75;
        public int danger;
        public int placementHeight;
        public int moveTime;

        public int bridge;
        public int downstack;

        public int cavities;
        public int cavitiesSq;
        public int cavitiesMove;
        public int overhangs;
        public int overhangsSq;
        public int overhangsMove;
        public int coveredCells;
        public int coveredCellsSq;
        public int rowTransitions;
        public int10 wellX;

        public bool noTspin;

        public static Weights Default => new() {
            clear1 = -500,
            clear2 = -375,
            clear3 = -160,
            clear4 = 800,
            tSpin1 = 220,
            tSpin2 = 1000,
            tSpin3 = 1270,
            tMini1 = -880,
            tMini2 = -500,
            perfect = 3800,
            tHole = 200,
            tstHole = 180,
            finHole = 150,
            b2bContinue = 340,
            b2bDestroy = -380,
            ren = 100,
            wastedT = -300,
            holdT = 10,
            bumpSum = 20,
            bumpSumSq = -10,
            maxHeightDiff = 0,
            holeDepth = 100,
            maxHeight = 20,
            top50 = -60,
            top75 = -300,
            danger = -20,
            placementHeight = 0,
            moveTime = -6,
            cavities = -400,
            cavitiesSq = -10,
            overhangs = -170,
            overhangsSq = -2,
            coveredCells = 0,
            coveredCellsSq = 0,
            rowTransitions = -5,
            wellX = new int10(150,-100,300,20,60,60,15,280,-110,140)
        };
        public static Weights Changed => new() {
            clear1 = -610,
            clear2 = -555,
            clear3 = -370,
            clear4 = 500,
            tSpin1 = 130,
            tSpin2 = 540,
            tSpin3 = 920,
            tMini1 = -680,
            tMini2 = -500,
            perfect = 2800,
            tHole = 140,
            tstHole = 160,
            finHole = 150,
            b2bContinue = 540,
            b2bDestroy = -350,
            ren = 60,
            wastedT = -250,
            holdT = 10,
            bumpSum = 130,
            bumpSumSq = -20,
            maxHeightDiff = 0,
            holeDepth = 50,
            maxHeight = -70,
            top50 = -50,
            top75 = -370,
            danger = -30,
            placementHeight = 0,
            moveTime = -4,
            cavities = -200,
            cavitiesSq = 0,
            overhangs = -90,
            overhangsSq = 0,
            coveredCells = -120,
            coveredCellsSq = 0,
            rowTransitions = -21,
            wellX = new int10(150,-100,300,20,60,60,15,280,-110,140)
        };
        public static Weights CCLike => new() {
            clear1 = -143,
            clear2 = -100,
            clear3 = -58,
            clear4 = 490,
            tSpin1 = 221,
            tSpin2 = 510,
            tSpin3 = 702,
            tMini1 = -58,
            tMini2 = 7,
            perfect = 999,
            tHole = 96,
            tstHole = 203,
            finHole = 200,
            b2bContinue = 52,
            b2bDestroy = -100,
            ren = 150,
            wastedT = -152,
            holdT = 0,
            bumpSum = -24,
            bumpSumSq = -7,
            maxHeightDiff = 0,
            holeDepth = 57,
            maxHeight = -39,
            top50 = -150,
            top75 = -511,
            danger = -11,
            placementHeight = 0,
            // closedHoles = -300,
            moveTime = -3,
            // bridge = -800,
            cavities = -173,
            cavitiesSq = -3,
            // cavitiesMove = -50,
            overhangs = -34,
            overhangsSq = -1,
            // overhangsMove = -30,
            coveredCells = -17,
            coveredCellsSq = -1,
            rowTransitions = -5,
            wellX = new int10(20, 23, 20, 50, 59, 21, 59, 10, -10, 24)
        };

        public static Weights AllClear => new() {
            perfect = 60000,
            moveTime = -200,
            maxHeight = -1000,
            placementHeight = -1000,
            clear1 = -1000,
            clear2 = 1400,
            clear3 = 1600,
            clear4 = 6000,
            noTspin = true
        };
    }
}