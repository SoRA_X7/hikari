namespace Hikari.AI.Eval {
    public readonly struct TspinHole {
        public readonly int lines;
        public readonly int x;
        public readonly int y;
        public readonly int r;

        public TspinHole(int lines, int x, int y, int r) {
            this.lines = lines;
            this.x = x;
            this.y = y;
            this.r = r;
        }
    }
}