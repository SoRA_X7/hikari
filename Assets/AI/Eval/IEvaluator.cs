using Hikari.Puzzle;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Hikari.AI.Eval {
    public interface IEvaluator {
        public Value EvaluateBoard([NoAlias] in SimpleBoard board, [NoAlias] in Weights weights,
            [NoAlias] in NativeArray<uint4x4> pieceShapes);

        public Reward EvaluateMove(int time, Piece piece,
            [NoAlias] in SimpleBoard board, [NoAlias] in SimpleLockResult lockResult,
            bool parentIsB2B, [NoAlias] in Weights weights);
    }
}