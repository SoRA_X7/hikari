using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Hikari.Puzzle;
using MessagePack;

namespace Hikari.AI.Utils.Export {
    public class BoardHistoryExporter {
        private readonly List<int[][]> history;
        private readonly string fileName;
        private bool write;

        public BoardHistoryExporter(string fileName = "Temp/board_history.txr") {
            history = new List<int[][]>();
            this.fileName = fileName;
            write = false;

            try {
                using var file = File.OpenRead(fileName);
                var existing = MessagePackSerializer.Deserialize<List<int[][]>>(file);
                history.AddRange(existing);
            } catch (IOException) { }
        }

        public void Append(Board board) {
            var bb = new int[40][];
            for (var i = 0; i < board.row.Length; i++) {
                bb[i] = board.row[i].cells.Select(colored => colored > 0 ? 1 : 0).ToArray();
            }

            history.Add(bb);
            write = true;
        }

        public void Export() {
            if (!write) return;
            using var file = File.OpenWrite(fileName);
            MessagePackSerializer.Serialize(file, history);
        }
    }
}