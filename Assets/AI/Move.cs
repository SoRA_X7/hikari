using Hikari.AI.Eval;
using Hikari.AI.Moves;

namespace Hikari.AI {
    public readonly struct Move {
        public readonly Path path;
        public readonly int nodes;
        public readonly int depth;
        public readonly Value eval;

        public Move(Path path, int nodes, int depth, Value eval) {
            this.path = path;
            this.nodes = nodes;
            this.depth = depth;
            this.eval = eval;
        }
    }
}