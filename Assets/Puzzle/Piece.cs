using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Hikari.Puzzle {
    public readonly struct Piece : IEquatable<Piece> {
        private byte Meta { get; }
        public PieceKind Kind => (PieceKind) (Meta & KindMask);
        public sbyte X { get; }
        public sbyte Y { get; }
        public sbyte Spin { get; }
        public TSpinStatus Tspin => (TSpinStatus) ((Meta & TspinMask) >> 4);
        public bool IsInvalid => (Meta & ValidMask) != 0;

        private const byte KindMask = (1 << 4) - 1;
        private const byte TspinMask = (1 << 7) - 1 - KindMask;
        private const byte ValidMask = 1 << 7;

        public static Piece Invalid => new(ValidMask, default, default, default);

        public Piece(PieceKind pieceKind)
            : this(pieceKind, 3, (sbyte) (pieceKind == PieceKind.I ? 17 : 18), 0) { }

        public Piece(PieceKind pieceKind, sbyte x, sbyte y, sbyte spin, TSpinStatus spinStatus = TSpinStatus.None) {
            X = x;
            Y = y;
            Spin = spin;
            Meta = (byte) ((byte) spinStatus << 4 | (byte) pieceKind);
        }

        private Piece(byte meta, sbyte x, sbyte y, sbyte spin) {
            Meta = meta;
            X = x;
            Y = y;
            Spin = spin;
        }

        public static readonly uint4x4[] NativeShapes = {
            math.transpose(new uint4x4(
                0, 0, 15, 0,
                4, 4, 4, 4,
                0, 15, 0, 0,
                2, 2, 2, 2)),
            math.transpose(new uint4x4(
                0, 6, 6, 0,
                0, 6, 6, 0,
                0, 6, 6, 0,
                0, 6, 6, 0)),
            math.transpose(new uint4x4(
                0, 7, 2, 0,
                2, 6, 2, 0,
                2, 7, 0, 0,
                2, 3, 2, 0)),
            math.transpose(new uint4x4(
                0, 7, 1, 0,
                2, 2, 6, 0,
                4, 7, 0, 0,
                3, 2, 2, 0)),
            math.transpose(new uint4x4(
                0, 7, 4, 0,
                6, 2, 2, 0,
                1, 7, 0, 0,
                2, 2, 3, 0)),
            math.transpose(new uint4x4(
                0, 3, 6, 0,
                4, 6, 2, 0,
                3, 6, 0, 0,
                2, 3, 1, 0)),
            math.transpose(new uint4x4(
                0, 6, 3, 0,
                2, 6, 4, 0,
                6, 3, 0, 0,
                1, 3, 2, 0))
        };

        public static readonly int2x4[] Cells = {
            new(0, 1, 2, 3, 2, 2, 2, 2),
            new(2, 2, 2, 2, 0, 1, 2, 3),
            new(0, 1, 2, 3, 1, 1, 1, 1),
            new(1, 1, 1, 1, 0, 1, 2, 3),

            new(1, 1, 2, 2, 1, 2, 1, 2),
            new(1, 1, 2, 2, 1, 2, 1, 2),
            new(1, 1, 2, 2, 1, 2, 1, 2),
            new(1, 1, 2, 2, 1, 2, 1, 2),

            new(0, 1, 2, 1, 1, 1, 1, 2),
            new(1, 1, 2, 1, 0, 1, 1, 2),
            new(1, 0, 1, 2, 0, 1, 1, 1),
            new(1, 0, 1, 1, 0, 1, 1, 2),

            new(0, 1, 2, 0, 1, 1, 1, 2),
            new(1, 1, 1, 2, 0, 1, 2, 2),
            new(2, 0, 1, 2, 0, 1, 1, 1),
            new(0, 1, 1, 1, 0, 0, 1, 2),

            new(0, 1, 2, 2, 1, 1, 1, 2),
            new(1, 2, 1, 1, 0, 0, 1, 2),
            new(0, 0, 1, 2, 0, 1, 1, 1),
            new(1, 1, 0, 1, 0, 1, 2, 2),

            new(0, 1, 1, 2, 1, 1, 2, 2),
            new(2, 1, 2, 1, 0, 1, 1, 2),
            new(0, 1, 1, 2, 0, 0, 1, 1),
            new(1, 0, 1, 0, 0, 1, 1, 2),

            new(1, 2, 0, 1, 1, 1, 2, 2),
            new(1, 1, 2, 2, 0, 1, 1, 2),
            new(1, 2, 0, 1, 0, 0, 1, 1),
            new(0, 0, 1, 1, 0, 1, 1, 2)
        };


        public ushort[] GetShape() {
            var shape = NativeShapes[(int) Kind][Spin];
            return new[] {(ushort) shape.x, (ushort) shape.y, (ushort) shape.z, (ushort) shape.w};
        }

        public int2x4 GetCells() {
            return Cells[(int) Kind * 4 + Spin] + new int2x4(X, X, X, X, Y, Y, Y, Y);
        }

        public int2x4 GetCells(in NativeArray<int2x4> cells) {
            return cells[(int) Kind * 4 + Spin] + new int2x4(X, X, X, X, Y, Y, Y, Y);
        }

        public IEnumerable<Vector2Int> GetCellPositions() {
            var shape = GetShape();
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < 4; j++) {
                    if ((shape[i] & (1 << j)) != 0) {
                        yield return new Vector2Int(X + j, Y + i);
                    }
                }
            }
        }

        public bool Equals(Piece other) {
            return Meta == other.Meta && X == other.X && Y == other.Y && Spin == other.Spin;
        }

        public override bool Equals(object obj) {
            return obj is Piece other && Equals(other);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (int) Kind;
                hashCode = (hashCode * 7) ^ X;
                hashCode = (hashCode * 3) ^ Y;
                hashCode = (hashCode * 3) ^ Spin;
                return hashCode;
            }
        }

        public override string ToString() {
            return IsInvalid ? "Invalid" : $"{Kind}{(Kind == PieceKind.T ? $"_{Tspin}" : "")} ({X},{Y}) {Spin}";
        }

        public static bool operator ==(Piece left, Piece right) {
            return left.Equals(right);
        }

        public static bool operator !=(Piece left, Piece right) {
            return !left.Equals(right);
        }

        public Piece WithOffset(Vector2Int offset) {
            return new Piece(Kind, (sbyte) (X + offset.x), (sbyte) (Y + offset.y), Spin, Tspin);
        }

        public Piece WithOffset(int x, int y) {
            return WithOffset(new Vector2Int(x, y));
        }

        public Piece WithOffset(int2 v) {
            return WithOffset(new Vector2Int(v.x, v.y));
        }

        public Piece WithTSpinStatus(TSpinStatus ts) {
            return new Piece(Kind, X, Y, Spin, ts);
        }

        public Piece WithSpin(sbyte s) {
            return new Piece(Kind, X, Y, s);
        }
    }
}