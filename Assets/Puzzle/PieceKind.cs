namespace Hikari.Puzzle {
    public enum PieceKind : byte {
        I,
        O,
        T,
        J,
        L,
        S,
        Z
    }
}