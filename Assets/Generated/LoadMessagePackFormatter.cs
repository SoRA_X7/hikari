using MessagePack;
using MessagePack.Resolvers;
using UnityEngine;

namespace Hikari.Generated {
    public static class LoadMessagePackFormatter {
        private static bool serializerRegistered;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        private static void Initialize() {
            if (serializerRegistered) return;
            
            StaticCompositeResolver.Instance.Register(
                GeneratedResolver.Instance,
                StandardResolver.Instance
            );

            var option = MessagePackSerializerOptions.Standard.WithResolver(StaticCompositeResolver.Instance);

            MessagePackSerializer.DefaultOptions = option;
            serializerRegistered = true;
        }

#if UNITY_EDITOR


        [UnityEditor.InitializeOnLoadMethod]
        static void EditorInitialize() {
            Initialize();
        }

#endif
    }
}