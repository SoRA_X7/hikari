using System;
using System.Collections.Generic;
using System.Linq;

namespace Hikari.Utils {
    public class WeightedIndex {
        private readonly List<float> weights = new List<float>();
        private float sum;

        public WeightedIndex(IEnumerable<float> weights) {
            this.weights.AddRange(weights);
            sum = this.weights.Sum();
        }

        public int Sample(float value) {
            if (value < 0 || value > 1) throw new ArgumentOutOfRangeException(nameof(value));
            value *= sum;
            for (var i = 0; i < weights.Count; i++) {
                value -= weights[i];
                if (value <= 0) return i;
            }

            return weights.Count - 1;
        }
    }
}