using System;
using System.Linq;
using UnityEngine;

namespace Hikari.Utils {
    public static class HikariClientSettings {
        public static bool Test1 { get; private set; }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
        private static void LoadArgs() {
            var args = Environment.GetCommandLineArgs();
            Test1 = args.Contains("-test1");
        }
    }
}